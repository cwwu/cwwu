<?php
/*-----------------------------------------------------------------------------------*/
/*	Create a new post type called portfolios
/*-----------------------------------------------------------------------------------*/

function create_post_type_portfolios() 
{
	$labels = array(
		'name' => __( 'Portfolio','notio'),
		'singular_name' => __( 'Portfolio','notio' ),
		'rewrite' => array('slug' => __( 'portfolios','notio' )),
		'add_new' => _x('Add New', 'portfolio', 'notio'),
		'add_new_item' => __('Add New Portfolio','notio'),
		'edit_item' => __('Edit Portfolio','notio'),
		'new_item' => __('New Portfolio','notio'),
		'view_item' => __('View Portfolio','notio'),
		'search_items' => __('Search Portfolio','notio'),
		'not_found' =>  __('No portfolios found','notio'),
		'not_found_in_trash' => __('No portfolios found in Trash','notio'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail')
	  ); 
	  
	  register_post_type('portfolio',$args);
	  flush_rewrite_rules();
}

$category_labels = array(
	'name' => __( 'Project Categories', 'notio'),
	'singular_name' => __( 'Project Category', 'notio'),
	'search_items' =>  __( 'Search Project Categories', 'notio'),
	'all_items' => __( 'All Project Categories', 'notio'),
	'parent_item' => __( 'Parent Project Category', 'notio'),
	'edit_item' => __( 'Edit Project Category', 'notio'),
	'update_item' => __( 'Update Project Category', 'notio'),
	'add_new_item' => __( 'Add New Project Category', 'notio'),
  'menu_name' => __( 'Project Categories', 'notio')
); 	

register_taxonomy("project-category", 
		array("portfolio"), 
		array("hierarchical" => true, 
				'labels' => $category_labels,
				'show_ui' => true,
    			'query_var' => true,
				'rewrite' => array( 'slug' => 'project-category' )
));

/* Initialize post types */
add_action( 'init', 'create_post_type_portfolios' );
?>